import "./App.css";
import { useState, useEffect, Fragment, useCallback, useRef, forwardRef } from "react";
import { Howl } from "howler";
import Swal from "sweetalert2/dist/sweetalert2.all.min.js";
import RouletteSound from "./audio/roulette.mp3";
import Jackpot from "./audio/Jackpot.mp3";
import ReactCanvasConfetti from "react-canvas-confetti";
import { Scrollbars } from "react-custom-scrollbars-2";
import { RouletteWheel } from 'react-casino-roulette';

import Sparkle from 'react-sparkle'
import { Animate } from "react-simple-animate";
import logoRoulette from "./images/logo.png";
import backgroundDefault from './images/bg.jpg';
import defaultBackground from './images/bg.jpg';

//material ui
import { styled } from '@mui/material/styles';
import Button from '@mui/material/Button';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Unstable_Grid2';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import Typography from '@mui/material/Typography';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';

import Dialog from '@mui/material/Dialog';
import ListItemText from '@mui/material/ListItemText';
import ListItem from '@mui/material/ListItem';
import List from '@mui/material/List';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import Slide from '@mui/material/Slide';

const canvasStyles = {
  position: "fixed",
  pointerEvents: "none",
  width: "100%",
  height: "100%",
  top: 0,
  left: 0,
};

function App() {
  const [backgroundImage, setBackgroundImage] = useState("");
  const [newDisplay, setNewDisplay] = useState();
  const [backgroundBlur, setBackgroundBlur] = useState(0);

  //roulette
  const [doneWheelSpin, setDoneWheelSpin] = useState(false);
  const [rouletteNumbers, setRouletteNumbers] = useState([]);

  //confetti
  const refAnimationInstance = useRef(null);

  const getInstance = useCallback((instance) => {
    refAnimationInstance.current = instance;
  }, []);

  const makeShot = useCallback((particleRatio, opts) => {
    refAnimationInstance.current &&
      refAnimationInstance.current({
        ...opts,
        origin: { y: 0.7 },
        particleCount: Math.floor(200 * particleRatio),
      });
  }, []);

  const fire = useCallback(() => {
    makeShot(0.25, {
      spread: 26,
      startVelocity: 55,
    });

    makeShot(0.2, {
      spread: 60,
    });

    makeShot(0.35, {
      spread: 100,
      decay: 0.91,
      scalar: 0.8,
    });

    makeShot(0.1, {
      spread: 120,
      startVelocity: 25,
      decay: 0.92,
      scalar: 1.2,
    });

    makeShot(0.1, {
      spread: 120,
      startVelocity: 45,
    });
  }, [makeShot]);

  const SoundPlay = (src) => {
    const sound = new Howl({
      src,
      html5: true,
    });
    sound.play();
  };

  //wheel
  const [start, setStart] = useState(false);
  const [winningBet, setWinningBet] = useState('-1');

  const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === 'dark' ? 'rgba(255, 0, 0, 0.0)' : 'rgba(255, 0, 0, 0.0)',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  }));

  //modal
  const Transition = forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
  });

  const [open, setOpen] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const spinWheel = () => {
    setDoneWheelSpin(false);
    var winningNumber = Math.floor(Math.random() * 29).toString();
    var numArray = [];
    numArray.push(...rouletteNumbers, winningNumber);

    setWinningBet(winningNumber);
    setStart(true);
    SoundPlay(RouletteSound);
    setNewDisplay(winningNumber);
    setRouletteNumbers(numArray);
    console.log(winningNumber);
  };

  const endSpin = () => {
    setDoneWheelSpin(true);
    setStart(false);
    setWinningBet('-1');
    fire();
    SoundPlay(Jackpot);
  };

  const employeeReset = async () => {
    Swal.fire({
      title: "Are you sure you want to reset?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "RESET",
    }).then((result) => {
      if (result.isConfirmed) {
        setDoneWheelSpin(false);
        setStart(false);
        setWinningBet('-1');
        setNewDisplay();
        setRouletteNumbers([]);
        Swal.fire({
          position: "center",
          icon: "success",
          title: "Reset",
          showConfirmButton: false,
          timer: 1500,
        });
      }
    });
  };

  const addBlurBackground = () => {
    setBackgroundBlur(backgroundBlur + 1);
  };

  const reduceBlurBackground = () => {
    if (backgroundBlur <= 0) {
      return
    } else {
      setBackgroundBlur(backgroundBlur - 1);
    }
  };

  const removeBlurBackground = () => {
    setBackgroundBlur(0);
  };

  const setDefaultBackground = () => {
    setBackgroundImage(defaultBackground);
  };

  const removeBackground = () => {
    setBackgroundImage();
  };

  useEffect(() => {
    const fileInput = document.getElementById("fileInput");
    fileInput?.addEventListener("change", (e) => {
      const file = fileInput?.files[0];
      const reader = new FileReader();
      reader?.addEventListener("load", () => {
        setBackgroundImage(reader?.result);
      });
      if (file) {
        reader?.readAsDataURL(file);
      }
      document.getElementById("fileInput").value = "";
    });
  }, [backgroundImage]);

  return (
    <Fragment>
      <Scrollbars>
        {
          start ?
            (<Sparkle
              minSize={10}
              maxSize={30}
              flickerSpeed={"fast"}
              overflowPx={-10}
            />)
            :
            (<Sparkle
              minSize={1}
              maxSize={10}
              overflowPx={-10}
            />)
        }

        <div className="lottery-container2" style={{ backgroundImage: `url(${backgroundImage ? backgroundImage : backgroundDefault})`, filter: `blur(${backgroundBlur}px)`, WebkitFilter: `blur(${backgroundBlur}px)` }}></div>
        <div className="main-container">

          <Box sx={{ flexGrow: 1, height: '20%' }} className="">
            <Grid container>
              <Grid
                xs={12}
                display="flex"
                alignItems="center"
                justifyContent="center"
              >
                <Box
                  component="img"
                  sx={{
                    height: "90%",
                    width: "35%",
                  }}
                  alt="The house from the offer."
                  src={logoRoulette}
                />
              </Grid>
            </Grid>
          </Box>

          <Box sx={{ flexGrow: 1 }} className="">
            <Grid container>
              <Grid xs={12} className="">
                <div>
                  <RouletteWheel start={start} winningBet={winningBet} onSpinningEnd={endSpin} />
                </div>

              </Grid>
            </Grid>
          </Box>

          <ReactCanvasConfetti refConfetti={getInstance} style={canvasStyles} />

          <Box sx={{ flexGrow: 1, height: '40%' }}>
            <Grid container>
              <Grid
                xs={12}
                display="flex"
                alignItems="center"
                justifyContent="center"
                className="button-box"
                paddingTop={2}
              >
                <Button className="spin-button" variant="contained" disabled={start} color="warning" onClick={() => spinWheel()}>SPIN</Button>
              </Grid>
              <Grid
                xs={12}
                className={doneWheelSpin === true ? 'display-container' : ""}
                display="flex"
                alignItems="center"
                justifyContent="center"
                paddingTop={2}
              >
                {doneWheelSpin === true ? (
                  <Fragment>
                    <Animate play start={{ opacity: 0 }} end={{ opacity: 1 }}>
                      <span className="display-number display-first-number">
                        {newDisplay}{" "}
                      </span>
                    </Animate>
                  </Fragment>
                ) : (
                  <></>
                )}
              </Grid>
            </Grid>
          </Box>
        </div>

        <div>
          <Accordion>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="panel1a-header"
            >
              <Typography>Options</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Box sx={{ flexGrow: 1 }}>
                <Grid container spacing={2}>
                  <Grid xs={12}>
                    <Item>
                      <form>
                        <label
                          htmlFor="fileInput"
                          style={{
                            display: "block",
                            cursor: "pointer",
                            background: "black",
                            padding: "10px",
                            color: "white",
                            textAlign: "center"
                          }}
                        >
                          Set Background Image
                        </label>
                        <input
                          className="d-none"
                          style={{ visibility: "hidden" }}
                          type="file"
                          name="fileInput"
                          id="fileInput"
                        />
                      </form>
                    </Item>
                  </Grid>

                  <Grid xs={3}>
                    <Item>
                      <div>
                        <Button variant="outlined" onClick={() => setDefaultBackground()}>
                          Add Default Background
                        </Button>
                      </div>
                    </Item>
                  </Grid>
                  <Grid xs={3}>
                    <Item>
                      <div>
                        <Button variant="outlined" onClick={() => removeBackground()}>
                          Remove Background
                        </Button>
                      </div>
                    </Item>
                  </Grid>
                  <Grid xs={2}>
                    <Item>
                      <div>
                        <Button variant="outlined" onClick={() => addBlurBackground()}>
                          Add Blur
                        </Button>
                      </div>
                    </Item>
                  </Grid>
                  <Grid xs={2}>
                    <Item>
                      <div>
                        <Button variant="outlined" onClick={() => reduceBlurBackground()}>
                          Reduce Blur
                        </Button>
                      </div>
                    </Item>
                  </Grid>
                  <Grid xs={2}>
                    <Item>
                      <div>
                        <Button variant="outlined" onClick={() => removeBlurBackground()}>
                          Remove Blur
                        </Button>
                      </div>
                    </Item>
                  </Grid>

                  <Grid xs={12}>
                    <Item>
                      <div>
                        <Button variant="outlined" onClick={handleClickOpen}>
                          Open Winner List
                        </Button>
                        <Dialog
                          fullScreen
                          open={open}
                          onClose={handleClose}
                          TransitionComponent={Transition}
                        >
                          <AppBar sx={{ position: 'relative' }}>
                            <Toolbar>
                              <IconButton
                                edge="start"
                                color="inherit"
                                onClick={handleClose}
                                aria-label="close"
                              >
                                <CloseIcon />
                              </IconButton>
                              <Typography sx={{ ml: 2, flex: 1 }} variant="h6" component="div">
                                Winner List
                              </Typography>
                              <Button autoFocus color="inherit" onClick={handleClose}>
                                close
                              </Button>
                            </Toolbar>
                          </AppBar>
                          <List>
                            {rouletteNumbers.length > 0 ? (
                              rouletteNumbers?.map((data, i) => (
                                <Fragment key={i}>
                                  <ListItem >
                                    <ListItemText
                                      primary={`${i + 1} - ${data}`}
                                    />
                                  </ListItem>
                                </Fragment>
                              ))
                            ) : (
                              <></>
                            )}
                          </List>
                        </Dialog>
                      </div>
                    </Item>
                  </Grid>
                  <Grid xs={12}>
                    <Item>
                      <div>
                        <Button variant="outlined" onClick={employeeReset} id="reset-button">
                          RESET
                        </Button>
                      </div>
                    </Item>
                  </Grid>
                </Grid>
              </Box>

            </AccordionDetails>
          </Accordion>
        </div>
      </Scrollbars>


    </Fragment >
  );
}

export default App;